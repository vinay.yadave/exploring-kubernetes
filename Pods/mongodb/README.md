
- Line 1-2: We’re using v1 of Kubernetes Pods API. Both apiVersion and kind are mandatory. That way, Kubernetes knows what we want to do (create a Pod) and which API version to use.

- Line 3-7: The next section is metadata. It provides information that does not influence how the Pod behaves. We used metadata to define the name of the Pod (db) and a few labels. While using the Controllers, labels will have a practical purpose.

- Line 8: The last section is the spec in which we defined a single container. We can have multiple containers defined as a Pod. Otherwise, the section would be written in singular (container without s).

- Line 12: In this case, the container is defined with the name (db), the image (mongo), the command that should be executed when the container starts (mongod)

- Line 13: Finally, the set of arguments. The arguments are defined as an array with, in this case, two elements (--rest and --httpinterface).