## Pods
- [Getting started with Pods](#getting-started-with-pods)
- [Imperative way to Create Pods](#imperative-way-to-create-pods)
- [Declarative way to define Pods](#declarative-way-to-define-pods)
- [Component and Stages in a Pod's Scheduling](#component-and-stages-in-a-pods-scheduling)
    - [Components](#components)
    - [Breakdown of Events](#breakdown-of-events)
    
    
#### Getting started with Pods
Pods are equivalent to bricks we use to build houses. Both are uneventful and not much by themselves. Yet, they are fundamental building blocks without which we could not construct the solution we are set to build.

> A **Pod** is a way to represent a running process in a cluster.

- A Pod encapsulates one or more containers. It provides a unique network IP, attaches storage resources, and also decides how containers should run. Everything in a Pod is tightly coupled.

##### Single and Multi Container Pods
> A Pod is a collection of containers that share the same purpose. However, doesn't mean that multi-container Pods are common. They are rare.

- Multi container Pods are good when one container acts as the main service and the rest serving as side-cars. Some of the use cases for this would include
       - Continuous Integration (CI)
       - Continuous Delivery (CD)
       - Continuous Deployment Process (CDP)
       

#### Imperative way to Create Pods
- I need to have my cluster running using `minikube start`
- A pod basically contains containrs running inside. So first create container and pod. `kubectl` allows us to create Pods with a single command.

```shell script
kubectl run db --image mongo \
    --generator "run-pod/v1"
```
- `db` - name of the pod

- We can verify
```shell script
kubectl get pods
# I can see still its configuring.
NAME   READY   STATUS              RESTARTS   AGE
db     0/1     ContainerCreating   0          31s

# On the second run after sometime, I can the pod running. 
kubectl get pods
NAME   READY   STATUS    RESTARTS   AGE
db     1/1     Running   0          87s

```

- Creating a pod with tomcat image.
```shell script
kubectl run tomcat --image tomcat \
--generator "run-pod/v1"

kubectl get pods
NAME     READY   STATUS    RESTARTS   AGE
db       1/1     Running   0          8m19s
tomcat   1/1     Running   0          59s

```

- After creating two pods, the cluster would look like
![Cluster with Pods](https://gitlab.com/hegdevenky/static-files/-/raw/master/devops/kubernetes/pods.png)

```shell script
# deleting a single pod
kubectl delete pod db

# deleting all pods
kubectl delete pod --all
```

- This way of creating pods is not the recommended way. We can use Declarative way to instruct kubernetes. 

#### Declarative way to define Pods
Even though a Pod can contain any number of containers, the most common use case is to use the **single container in a Pod model**. In such a case, a Pod is a wrapper around one container. From Kubernetes’ perspective, a Pod is the smallest unit.

> We cannot tell Kubernetes to run a container. Instead, we ask it to create a Pod that wraps around a container.

- YAML files can be used to define the Pods Declarative way.
- Consider the example of creating Pod with [MonogoDB] and [Tomact] 

- To **create Pod from YAML file** run the below command
```shell script
kubectl create -f <name-of-file-with-ext>
```
- Running the [mongodb](mongodb/) and [tomcat](tomact/) pods.

`kubectl create -f tomact/tomcat.yml`
`kubectl create -f mongodb/db.yml`

```shell script
 % kubectl get pods
NAME     READY   STATUS    RESTARTS   AGE
db       1/1     Running   0          43s
tomcat   1/1     Running   0          9s
```
- retrieve a bit more information by specifying wide output.

`kubectl get pods -o wide`
```shell script
Pods % kubectl get pods -o wide           
NAME     READY   STATUS    RESTARTS   AGE     IP           NODE       NOMINATED NODE   READINESS GATES
db       1/1     Running   0          3m12s   172.17.0.4   minikube   <none>           <none>
tomcat   1/1     Running   0          2m38s   172.17.0.5   minikube   <none>           <none>
```

>  We can get the information of pods either in json or yaml format by using below commands. Json format is more informative. However, yaml is more human readable.
> `kubectl get pods -o json` or `kubectl get pods -o yaml`

-The `describe` sub-command of `kubectl` returns details of the specified resource.

Syntax - `kubectl describe <resource_kind> <resource_name>`

Example
```shell script
Pods % kubectl describe pod tomcat                                  

Name:         tomcat
Namespace:    default
Priority:     0
Node:         minikube/192.168.99.102
Start Time:   Wed, 22 Apr 2020 18:40:32 -0400
Labels:       type=server
              vendor=Apache
Annotations:  <none>
Status:       Running
IP:           172.17.0.5
Containers:
  tomcat:
    Container ID:  docker://8c90800b51fb3a8022a765acfdb9b3e48b6000f564fa46479a37dfbc65bd131b
    Image:         tomcat:jdk8-openjdk-slim
    Image ID:      docker-pullable://tomcat@sha256:fc5007bef7e9fc0e444ba6c7db3f5399670fb9d1a41971b8e89c178e27b0c874
    Port:          <none>
    Host Port:     <none>
    Command:
      catalina.sh
      run
    State:          Running
      Started:      Wed, 22 Apr 2020 18:40:33 -0400
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-7skdf (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             True 
  ContainersReady   True 
  PodScheduled      True 
Volumes:
  default-token-7skdf:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-7skdf
    Optional:    false
QoS Class:       BestEffort
Node-Selectors:  <none>
Tolerations:     node.kubernetes.io/not-ready:NoExecute for 300s
                 node.kubernetes.io/unreachable:NoExecute for 300s
Events:
  Type    Reason     Age    From               Message
  ----    ------     ----   ----               -------
  Normal  Scheduled  6m29s  default-scheduler  Successfully assigned default/tomcat to minikube
  Normal  Pulled     6m29s  kubelet, minikube  Container image "tomcat:jdk8-openjdk-slim" already present on machine
  Normal  Created    6m29s  kubelet, minikube  Created container tomcat
  Normal  Started    6m28s  kubelet, minikube  Started container tomcat

```

- Command to delete the Pod created from YAML file is

`kubectl delete -f <filename.yml>`

`kubectl delete pods --all` would all work.

#### Component and Stages in a Pod's Scheduling

- There are many stages and components involved in a Pod's creation.

##### Components
1. API Server
    - The API server is the central component of a Kubernetes cluster and it runs on the master node. (Since we are using Minikube, both master and worker nodes are baked into the same virtual machine. However, a more serious Kubernetes cluster should have the two separated on different hosts.)
    -  Most of the coordination in Kubernetes consists of a component writing to the API Server resource that another component is watching. The second component will then react to changes almost immediately.

2. Scheduler
    - The _scheduler_ also runs on the master node. It's jobs are
        - Watch for unassigned Pods
        - Assign a pod to a node which has available resources such as CPU and memory as per the Pod requirements.

3. Kubelet
    - Kublet runs on each node. It's function is to make sure that assigned pods are running on the node.

##### Breakdown of Events

Following sequene of events that transpired with the `kubectl create -f <filename.yml>` or `kubectl create -f tomcat.yml` command.

1. Kubernetes client i.e `kubectl` sends a request to the API Server requesting creation of a Pod defined in the YAML file.
2.Since the scheduler is watching the API server for new events, it detected that there is an unassigned Pod.
3. Scheduler decides which node it should assign the pod and sends the information to the API Server.
4. Kubelet detects the pod assigned to it's node. Now, most of the work takes place here.
5. Kubelet sends a request to Dovker for creating containers from the Pod. (In this case, the Pod defines a single container based on the `tomcat` image).
6. Kubelets sends a request to API Server notifying that the Pod was created successfully. 

Below sequence diagram helps to understand better. 

![](https://gitlab.com/hegdevenky/static-files/-/raw/master/devops/kubernetes/sequence.png)

##### Describing Resource

- In many cases, it is more useful to describe resources by referencing the file that defines them, that way we can always ensure that we are seeing appropriate information, which can be get using
```shell script
kubectl describe -f <.yml>

 Pods % kubectl describe -f tomact/tomcat.yml 
Name:         tomcat
Namespace:    default
Priority:     0
Node:         minikube/192.168.99.103
Start Time:   Thu, 23 Apr 2020 20:41:02 -0400
Labels:       type=server
              vendor=Apache
Annotations:  <none>
Status:       Running
IP:           172.17.0.5
Containers:
  tomcat:
    Container ID:  docker://91f549e0c6af1797a949fafcc2a73fc9d31351c77ef9104fe1957cadb1fcbd43
    Image:         tomcat:jdk8-openjdk-slim
    Image ID:      docker-pullable://tomcat@sha256:fc5007bef7e9fc0e444ba6c7db3f5399670fb9d1a41971b8e89c178e27b0c874
    Port:          <none>
    Host Port:     <none>
    Command:
      catalina.sh
      run
    State:          Running
      Started:      Thu, 23 Apr 2020 20:43:08 -0400
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-rvdl9 (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             True 
  ContainersReady   True 
  PodScheduled      True 
Volumes:
  default-token-rvdl9:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-rvdl9
    Optional:    false
QoS Class:       BestEffort
Node-Selectors:  <none>
Tolerations:     node.kubernetes.io/not-ready:NoExecute for 300s
                 node.kubernetes.io/unreachable:NoExecute for 300s
Events:
  Type    Reason     Age    From               Message
  ----    ------     ----   ----               -------
  Normal  Scheduled  11m    default-scheduler  Successfully assigned default/tomcat to minikube
  Normal  Pulling    11m    kubelet, minikube  Pulling image "tomcat:jdk8-openjdk-slim"
  Normal  Pulled     8m57s  kubelet, minikube  Successfully pulled image "tomcat:jdk8-openjdk-slim"
  Normal  Created    8m56s  kubelet, minikube  Created container tomcat
  Normal  Started    8m56s  kubelet, minikube  Started container tomcat
```

##### Executing process inside container
- Just as with Docker, we can execute a new process inside a running container inside a Pod.
```shell script
kubectl exec db ps aux
```

- The above command instructs Kubernetes to execute a process inside the first container of the Pod db. As the Pod defines only one container, that will be the first container. Also, we can use `--control` or `-c` flag to specifyt he name of the container. 

- Apart from using Pods as the reference, `kubectl exec` is almost the same as the `docker container exec` command. The significant difference is that `kubectl` allows us to **execute a process in a container running in any node inside a cluster**, while `docker container exec` is **limited to containers running on a specific node**.

- we can enter into it. For example, we can make the execution interactive with `-i` (stdin) and `-t` (terminal) arguments and run shell inside a container.
```shell script
kubectl exec -it db sh
```

##### Logs
- Logs should be usually shipped from containers to a central location.
- Command to get log from container in a Pod is `kubectl logs <name-of-pod>`. With the flag `-f` or `-follow` we cam follow logs in real time.
```shell script
 kubectl logs tomcat
24-Apr-2020 00:43:09.320 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Server version name:   Apache Tomcat/8.5.54
24-Apr-2020 00:43:09.323 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Server built:          Apr 3 2020 14:06:10 UTC
24-Apr-2020 00:43:09.324 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Server version number: 8.5.54.0
24-Apr-2020 00:43:09.324 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log OS Name:               Linux
24-Apr-2020 00:43:09.324 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log OS Version:            4.19.107
24-Apr-2020 00:43:09.325 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Architecture:          amd64
24-Apr-2020 00:43:09.325 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Java Home:             /usr/local/openjdk-8/jre
24-Apr-2020 00:43:09.325 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log JVM Version:           1.8.0_252-b09
24-Apr-2020 00:43:09.326 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log JVM Vendor:            Oracle Corporation
24-Apr-2020 00:43:09.326 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log CATALINA_BASE:         /usr/local/tomcat
24-Apr-2020 00:43:09.326 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log CATALINA_HOME:         /usr/local/tomcat
24-Apr-2020 00:43:09.327 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Djava.util.logging.config.file=/usr/local/tomcat/conf/logging.properties
24-Apr-2020 00:43:09.327 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager
24-Apr-2020 00:43:09.327 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Djdk.tls.ephemeralDHKeySize=2048
24-Apr-2020 00:43:09.328 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Djava.protocol.handler.pkgs=org.apache.catalina.webresources
24-Apr-2020 00:43:09.328 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Dorg.apache.catalina.security.SecurityListener.UMASK=0027
24-Apr-2020 00:43:09.329 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Dignore.endorsed.dirs=
24-Apr-2020 00:43:09.329 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Dcatalina.base=/usr/local/tomcat
24-Apr-2020 00:43:09.329 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Dcatalina.home=/usr/local/tomcat
24-Apr-2020 00:43:09.330 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Djava.io.tmpdir=/usr/local/tomcat/temp
24-Apr-2020 00:43:09.330 INFO [main] org.apache.catalina.core.AprLifecycleListener.lifecycleEvent Loaded APR based Apache Tomcat Native library [1.2.23] using APR version [1.6.5].
24-Apr-2020 00:43:09.331 INFO [main] org.apache.catalina.core.AprLifecycleListener.lifecycleEvent APR capabilities: IPv6 [true], sendfile [true], accept filters [false], random [true].
24-Apr-2020 00:43:09.331 INFO [main] org.apache.catalina.core.AprLifecycleListener.lifecycleEvent APR/OpenSSL configuration: useAprConnector [false], useOpenSSL [true]
24-Apr-2020 00:43:09.338 INFO [main] org.apache.catalina.core.AprLifecycleListener.initializeSSL OpenSSL successfully initialized [OpenSSL 1.1.1d  10 Sep 2019]
24-Apr-2020 00:43:09.449 INFO [main] org.apache.coyote.AbstractProtocol.init Initializing ProtocolHandler ["http-nio-8080"]
24-Apr-2020 00:43:09.464 INFO [main] org.apache.tomcat.util.net.NioSelectorPool.getSharedSelector Using a shared selector for servlet write/read
24-Apr-2020 00:43:09.509 INFO [main] org.apache.catalina.startup.Catalina.load Initialization processed in 899 ms
24-Apr-2020 00:43:09.550 INFO [main] org.apache.catalina.core.StandardService.startInternal Starting service [Catalina]
24-Apr-2020 00:43:09.551 INFO [main] org.apache.catalina.core.StandardEngine.startInternal Starting Servlet Engine: Apache Tomcat/8.5.54
24-Apr-2020 00:43:09.564 INFO [main] org.apache.coyote.AbstractProtocol.start Starting ProtocolHandler ["http-nio-8080"]
24-Apr-2020 00:43:09.575 INFO [main] org.apache.catalina.startup.Catalina.start Server startup in 63 ms
vshantha@vshantha-Mac Pods % clear
vshantha@vshantha-Mac Pods % clear
vshantha@vshantha-Mac Pods % clear
vshantha@vshantha-Mac Pods % kubectl logs tomcat
24-Apr-2020 00:43:09.320 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Server version name:   Apache Tomcat/8.5.54
24-Apr-2020 00:43:09.323 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Server built:          Apr 3 2020 14:06:10 UTC
24-Apr-2020 00:43:09.324 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Server version number: 8.5.54.0
24-Apr-2020 00:43:09.324 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log OS Name:               Linux
24-Apr-2020 00:43:09.324 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log OS Version:            4.19.107
24-Apr-2020 00:43:09.325 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Architecture:          amd64
24-Apr-2020 00:43:09.325 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Java Home:             /usr/local/openjdk-8/jre
24-Apr-2020 00:43:09.325 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log JVM Version:           1.8.0_252-b09
24-Apr-2020 00:43:09.326 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log JVM Vendor:            Oracle Corporation
24-Apr-2020 00:43:09.326 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log CATALINA_BASE:         /usr/local/tomcat
24-Apr-2020 00:43:09.326 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log CATALINA_HOME:         /usr/local/tomcat
24-Apr-2020 00:43:09.327 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Djava.util.logging.config.file=/usr/local/tomcat/conf/logging.properties
24-Apr-2020 00:43:09.327 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager
24-Apr-2020 00:43:09.327 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Djdk.tls.ephemeralDHKeySize=2048
24-Apr-2020 00:43:09.328 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Djava.protocol.handler.pkgs=org.apache.catalina.webresources
24-Apr-2020 00:43:09.328 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Dorg.apache.catalina.security.SecurityListener.UMASK=0027
24-Apr-2020 00:43:09.329 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Dignore.endorsed.dirs=
24-Apr-2020 00:43:09.329 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Dcatalina.base=/usr/local/tomcat
24-Apr-2020 00:43:09.329 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Dcatalina.home=/usr/local/tomcat
24-Apr-2020 00:43:09.330 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Djava.io.tmpdir=/usr/local/tomcat/temp
24-Apr-2020 00:43:09.330 INFO [main] org.apache.catalina.core.AprLifecycleListener.lifecycleEvent Loaded APR based Apache Tomcat Native library [1.2.23] using APR version [1.6.5].
24-Apr-2020 00:43:09.331 INFO [main] org.apache.catalina.core.AprLifecycleListener.lifecycleEvent APR capabilities: IPv6 [true], sendfile [true], accept filters [false], random [true].
24-Apr-2020 00:43:09.331 INFO [main] org.apache.catalina.core.AprLifecycleListener.lifecycleEvent APR/OpenSSL configuration: useAprConnector [false], useOpenSSL [true]
24-Apr-2020 00:43:09.338 INFO [main] org.apache.catalina.core.AprLifecycleListener.initializeSSL OpenSSL successfully initialized [OpenSSL 1.1.1d  10 Sep 2019]
24-Apr-2020 00:43:09.449 INFO [main] org.apache.coyote.AbstractProtocol.init Initializing ProtocolHandler ["http-nio-8080"]
24-Apr-2020 00:43:09.464 INFO [main] org.apache.tomcat.util.net.NioSelectorPool.getSharedSelector Using a shared selector for servlet write/read
24-Apr-2020 00:43:09.509 INFO [main] org.apache.catalina.startup.Catalina.load Initialization processed in 899 ms
24-Apr-2020 00:43:09.550 INFO [main] org.apache.catalina.core.StandardService.startInternal Starting service [Catalina]
24-Apr-2020 00:43:09.551 INFO [main] org.apache.catalina.core.StandardEngine.startInternal Starting Servlet Engine: Apache Tomcat/8.5.54
24-Apr-2020 00:43:09.564 INFO [main] org.apache.coyote.AbstractProtocol.start Starting ProtocolHandler ["http-nio-8080"]
24-Apr-2020 00:43:09.575 INFO [main] org.apache.catalina.startup.Catalina.start Server startup in 63 ms
```

##### Containers failing
- Everytime a container fails, Kubernetes restarts the container. 
```shell script
 Pods % kubectl exec -it db pkill mongod # killing the process mongod inside the container of Pod db

# Now, I can see that STATUS indicating restart completed.
vshantha@vshantha-Mac Pods % kubectl get pods                
NAME     READY   STATUS      RESTARTS   AGE
db       0/1     Completed   0          16m
tomcat   1/1     Running     0          16m

# next second, I can see the RESTARTS count 1 and Status Running.  
vshantha@vshantha-Mac Pods % kubectl get pods
NAME     READY   STATUS    RESTARTS   AGE
db       1/1     Running   1          17m
tomcat   1/1     Running   0          16m
```
- Below image describes the container failing.

![Alt Text](https://gitlab.com/hegdevenky/static-files/-/raw/master/devops/kubernetes/container-failing.png)

- We can follow similar way of specifying the resources while deleting the pod as well, instead of Pod name.
```shell script
Pods % kubectl delete -f mongodb/db.yml
pod "db" deleted
vshantha@vshantha-Mac Pods % kubectl get pods
NAME     READY   STATUS    RESTARTS   AGE
tomcat   1/1     Running   0          34m
```

#### Running multiple Containers in a pod.
- Pods are designed to run multiple cooperative processes that should act as a cohesive unit. Those processes are wrapped in containers.
- All the containers that form a Pod are running on the same machine. A Pod cannot be split across multiple nodes.
- All the processes (containers) inside a Pod share the same set of resources, and they can communicate with each other through localhost. One of those shared resources is storage.
- A volume (think of it as a directory with shareable data) defined in a Pod can be accessed by all the containers thus allowing them all to share the same data.


